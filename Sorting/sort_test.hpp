#ifndef SORT_TEST_H
#define SORT_TEST_H
#define NO_OF_TESTS 100
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>
using namespace std;
template <class Container = vector<int> >
class testSort
{
	typedef typename Container::iterator iter;
	void (*func)(iter first, iter last);
	clock_t startTime, endTime;
	double timeTaken;
public:
	testSort(void (*sortFunc)(iter first, iter last))
	{
		func = sortFunc;
	}
	
	bool isCorrect(iter first, iter last);
	
	void randomFill(iter first, iter last);
	
	double test(iter first, iter last);
	
	double randomTest(iter first, iter last);
};

template <class Container> bool testSort<Container>::isCorrect(iter first, iter last)
{
	for (int i = 0; i < NO_OF_TESTS; i++)
	{
		randomFill(first, last);
		Container result(last - first);
		copy(first, last, result.begin());
		(*func)(first, last);
		sort(result.begin(), result.end());
		if (!equal(result.begin(), result.end(), first))
		{
			return false;
		}
	}
	return true;
}

template <class Container> void testSort<Container>::randomFill(iter first, iter last)
{
	srand(time(NULL));
	for (iter i = first; i != last; i++)
		*i = (rand()) % 32678;
}

template <class Container> double testSort<Container>::test(iter first, iter last)
{
	startTime = clock();
	(*func)(first, last);
	endTime = clock();
	timeTaken = (double)(endTime - startTime) / CLOCKS_PER_SEC;
	return timeTaken;
}

template <class Container> double testSort<Container>::randomTest(iter first, iter last)
{
	randomFill(first, last);
	return test(first, last);
}
#endif
