#include "sort_test.hpp"
using namespace std;

/*
	Bubble Sort
*/
template <class iter>
void bubbleSort(iter first, iter last)
{
	for (iter i = first; i != last - 1; i++)
		for (iter j = first; j != last - i + first - 1; j++)
			if (*j > *(j + 1))
				iter_swap(j, j + 1);
}
/*
	Selection Sort
*/
template <class iter>
void selectionSort(iter first, iter last)
{
	for (iter i = first; i != last - 1; i++)
		iter_swap(min_element(i, last), i);
}

/*
	Merge Sort
*/
template <class iter>
void merge(iter first, iter middle, iter last)
{
	vector<typename iter::value_type> res;
	iter cur1 = first;
	iter cur2 = middle;
	while (cur1 != middle && cur2 != last)
	{
		if (*cur1 > *cur2)
		{
			res.push_back(*cur2);
			cur2++;
		}
		else
		{
			res.push_back(*cur1);
			cur1++;
		}
	}	
	while (cur1 != middle)
	{
		res.push_back(*cur1);
		cur1++;
	}
	while (cur2 != last)
	{
		res.push_back(*cur2);
		cur2++;
	}
	for (iter i = res.begin(), j = first; i != res.end(); i++, j++)
		*j = *i;
}

template <class iter>
void mergeSort(iter first, iter last)
{
	if ((last - first) > 1)
	{
		iter mid = first + (last - first) / 2;
		mergeSort(first, mid);
		mergeSort(mid, last);
		merge(first, mid, last);
	}
}


/*
	Quick Sort
*/

template <class iter>
iter hoarePartition(iter first, iter last)
{
	iter key = first;
	iter i = first;
	iter j = last;
	while (i < j)
	{
		do
		{
			i++;
		}
		while (i != last && *i <= *key);
		do
		{
			j--;
		}
		while (j != first && *j > *key);
		if (i < j)
			iter_swap(i, j);
		else
		{
			iter_swap(first, j);
			return j;
		}
	}
}
template <class iter>
iter partition(iter first, iter last)
{
	iter key = last - 1;
	iter i = first - 1;
	for (iter j = first; j != last - 1; j++)
	{
		if (*j <= *key)
		{
			i++;
			iter_swap(i, j);
		}
	}
	i++;
	iter_swap(i, key);
	return i;
}

template <class iter>
void quickSort(iter first, iter last)
{
	if ((last - first) > 1)
	{
		iter p = hoarePartition(first, last);
		quickSort(first, p);
		quickSort(p + 1, last);
	}
}


int main()
{
	vector<int> v(1000);
	/*
	testSort<> t1(bubbleSort);
	if (!t1.isCorrect(v.begin(), v.end()))
		cout << "Incorrect sorting\n";
	cout << t1.randomTest(v.begin(), v.end()) << endl;
	testSort<> t2(selectionSort);
	if (!t2.isCorrect(v.begin(), v.end()))
		cout << "Incorrect sorting\n";
	cout << t2.randomTest(v.begin(), v.end()) << endl;
	*/
	testSort<> t3(mergeSort);
	if (!t3.isCorrect(v.begin(), v.end()))
		cout << "Incorrect sorting Merge\n";
	cout << t3.randomTest(v.begin(), v.end()) << endl;

	testSort<> t4(quickSort);
	if (!t4.isCorrect(v.begin(), v.end()))
		cout << "Incorrect sorting Quick Sort\n";
	cout << t4.randomTest(v.begin(), v.end()) << endl;
	return 0;
}
