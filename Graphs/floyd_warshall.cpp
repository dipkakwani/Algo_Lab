#include<bits/stdc++.h>
#define INF 99999
using namespace std;
void floyd_warshall(vector< vector<int> >& graph, int v)
{
    for (int k = 0; k < v; k++)
        for (int i = 0; i < v; i++)
            for (int j = 0; j < v; j++)
                if (graph[i][j] > graph[i][k] + graph[k][j])
                    graph[i][j] = graph[i][k] + graph[k][j];
}
int main()
{
    int v, e;
    cout << "Enter no. of vertices and edges\n";
    cin >> v >> e;
    cout << "Enter the edes (e1 e2 weight)\n";
    int e1, e2, w;
    vector< vector<int> > graph(v, vector<int>(v));
    for (int i = 0; i < v; i++)
        for (int j = 0; j < v; j++)
            graph[i][j] = INF;
   
    for (int i = 0; i < e; i++)
    {
        cin >> e1 >> e2 >> w;
        graph[e1 - 1][e2 - 1] = w;
    }
    for (int i = 0; i < v; i++)
        graph[i][i] = 0;       
    floyd_warshall(graph, v);
    cout << "The final distance matrix\n";
    for (int i = 0; i < v; i++)
    {
        for (int j = 0; j < v; j++)
        {
            if (graph[i][j] != INF)
                cout << graph[i][j] << "  ";
            else
                cout << "INF ";
        }
        cout << endl;
    }
    return 0;
}
