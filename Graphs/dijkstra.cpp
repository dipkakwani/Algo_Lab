//Author : Diptanshu Kakwani
#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int, int> pii;
typedef vector< pii > vpii;
typedef vector< vpii > g;

#define MAX 100000
vector<int> dijkstra(g& graph, int source)
{
	int v = graph.size();

	set<pii> Q;
	vector<int> distance(v);
	for (int i = 0; i < v; i++)
		distance[i] = MAX;	//MAX weight
	distance[source] = 0;
	
	Q.insert(pii(0, source));
	while (!Q.empty())
	{
		pii temp = *Q.begin();
		Q.erase(Q.begin());
		int dist = temp.first;
		int curVertex = temp.second;
		for (auto i = graph[curVertex].begin(); i != graph[curVertex].end(); i++)
		{
			int vertex = (*i).first;
			int weight = (*i).second;
			if (distance[vertex] > dist + weight)		//Relax edge
			{
				if (distance[vertex] != MAX)
					Q.erase(Q.find(pii(distance[vertex], vertex)));
				distance[vertex] = dist + weight;
				Q.insert(pii(distance[vertex], vertex));
			}
		}
	}
	return distance;
}
int main()
{
   	int v, e;
	cin >> v >> e;
	g graph(v);
	for (int i = 0; i < e; i++)
	{
		int a, b, c;
		cin >> a >> b >> c;
		graph[a - 1].push_back(pair<int, int>(b - 1, c));
		graph[b - 1].push_back(pair<int, int>(a - 1, c));
	}
    vector<int> dist = dijkstra(graph, 0);
    for (auto it = dist.begin(); it != dist.end(); it++)
   		cout << *it << "  ";
    cout << endl;
}
