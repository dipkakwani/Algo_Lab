#include<bits/stdc++.h>
using namespace std;
void warshall(vector< vector<bool> >& graph, int v)
{
    for (int k = 0; k < v; k++)
        for (int i = 0; i < v; i++)
            for (int j = 0; j < v; j++)
                graph[i][j] = graph[i][j] || (graph[i][k] && graph[k][j]);
}
int main()
{
    int v, e;
    char choice;
    bool undirected = false;
    cout << "Is the graph directed or undirected(d/u)\n";
    cin >> choice;
    if (choice == 'u')
        undirected = true;
    cout << "Enter no. of vertices and edges\n";
    cin >> v >> e;
    cout << "Enter the edes (e1 e2)\n";
    int e1, e2;
    vector< vector<bool> > graph(v, vector<bool>(v)) ;
    for (int i = 0; i < e; i++)
    {
        cin >> e1 >> e2;
        graph[e1 - 1][e2 - 1] = 1;
        if (undirected)
            graph[e2 - 1][e1 - 1] = 1;
    }
    warshall(graph, v);
    cout << "The transitive closure\n";
    for (int i = 0; i < v; i++)
    {
        for (int j = 0; j < v; j++)
            cout << graph[i][j] << "  ";
        cout << endl;
    }
}
